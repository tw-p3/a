// Global variables
let map;
let restaurantData = []; // To store fetched restaurant data

// Initialize Google Maps
function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 0, lng: 0 },
    zoom: 10,
  });
}

// Handle form submission on contact-us.html
const contactForm = document.getElementById("contact-form");
if (contactForm) {
  contactForm.addEventListener("submit", function (event) {
    event.preventDefault();

    // Get form input values
    const nameInput = document.getElementById("name-input");
    const emailInput = document.getElementById("email-input");
    const messageInput = document.getElementById("message-input");

    // Send email or perform other actions
    // You can use this code as a starting point and customize it to fit your needs
    sendEmail(nameInput.value, emailInput.value, messageInput.value);

    // Reset the form
    contactForm.reset();
  });
}

// Handle team member data on about-us.html
const teamMembers = [
  {
    name: "April Gilmore",
    role: "Fullstack",
    image: "team-member1.jpg", // Replace with the actual image URL
  },
  {
    name: "Kanchana Iyer",
    role: "Frontend",
    image: "team-member2.jpg", // Replace with the actual image URL
  },
  {
    name: "Peter Itodo",
    role: "Fullstack",
    image: "team-member2.jpg", // Replace with the actual image URL
  },
  {
    name: "Manuel Coronel",
    role: "Backend",
    image: "team-member2.jpg", // Replace with the actual image URL
  },
  {
    name: "Maimouna Diallo",
    role: "Fullstack",
    image: "team-member2.jpg", // Replace with the actual image URL
  },
];

const teamMemberContainer = document.getElementById("team-members");
if (teamMemberContainer) {
  teamMembers.forEach((member) => {
    const memberCard = createTeamMemberCard(member);
    teamMemberContainer.appendChild(memberCard);
  });
}

// Function to create team member card
function createTeamMemberCard(member) {
  const card = document.createElement("div");
  card.classList.add("team-member-card");

  const image = document.createElement("img");
  image.src = member.image;
  image.alt = member.name;
  card.appendChild(image);

  const name = document.createElement("h3");
  name.textContent = member.name;
  card.appendChild(name);

  const role = document.createElement("p");
  role.textContent = member.role;
  card.appendChild(role);

  return card;
}

// Function to send email on contact-us.html
function sendEmail(name, email, message) {
  // You can implement your logic to send an email here
  // This is just a placeholder function
  console.log(`Sending email to ${email}`);
  console.log(`Name: ${name}`);
  console.log(`Message: ${message}`);
}

// Function to display company information on company-profile.html
function displayCompanyProfile() {
  // You can implement your logic to display company information here
  // This is just a placeholder function
  console.log("Displaying company profile");
}

// Fetch nearby restaurants using the Google Places API on index.html
const locationForm = document.getElementById("location-form");
if (locationForm) {
  locationForm.addEventListener("submit", function (event) {
    event.preventDefault();

    const locationInput = document.getElementById("location-input");
    const location = locationInput.value;

    fetchRestaurants(location);
  });
}

// Fetch nearby restaurants using the Google Places API
function fetchRestaurants(location) {
  // Replace "YOUR_API_KEY" with your actual Google Places API key
  const apiKey = "AIzaSyCs8oTQ0Lh2O4hjHNftPTxWpgAeeqJd9X8";
  const url = `https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurants+in+${location}&key=${apiKey}`;

  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      restaurantData = data.results; // Store the fetched restaurant data
      displayRestaurants(restaurantData); // Display the restaurants on the page
    })
    .catch((error) => {
      console.log("Error fetching restaurants:", error);
    });
}

// Display the list of restaurants on the page
function displayRestaurants(restaurants) {
  const restaurantList = document.getElementById("restaurant-list");

  if (restaurants && restaurants.length > 0) {
    restaurantList.innerHTML = ""; // Clear previous results

    restaurants.forEach((restaurant) => {
      const listItem = createRestaurantListItem(restaurant);
      restaurantList.appendChild(listItem);
    });
  } else {
    restaurantList.innerHTML = "No restaurants found.";
  }
}

// Create a list item for a restaurant
function createRestaurantListItem(restaurant) {
  const listItem = document.createElement("li");
  listItem.classList.add("list-group-item");

  const image = document.createElement("img");
  image.src = "restaurant.jpg"; // Replace with actual image URL
  image.alt = restaurant.name;
  image.classList.add("img-fluid", "rounded");
  listItem.appendChild(image);

  const name = document.createElement("h3");
  name.textContent = restaurant.name;
  listItem.appendChild(name);

  const address = document.createElement("p");
  address.textContent = "Address: " + restaurant.formatted_address;
  listItem.appendChild(address);

  const menuHeading = document.createElement("p");
  menuHeading.textContent = "Menu:";
  listItem.appendChild(menuHeading);

  const menuList = document.createElement("ul");
  restaurant.menu.forEach((menuItem) => {
    const menuListItem = document.createElement("li");
    menuListItem.textContent = menuItem;
    menuList.appendChild(menuListItem);
  });
  listItem.appendChild(menuList);

  return listItem;
}
